﻿using Interlogic.Employees.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interlogic.Employees.DAL
{
    public class EmployeeAdapter
    {
        private readonly EmployeeContext _dbContext;

        public EmployeeAdapter(EmployeeContext context)
        {
            this._dbContext = context;
        }

        public IEnumerable<Employee> GetAll()
        {
            return _dbContext.Employees;
        }

        public Employee GetEmployeeByLogin(string login)
        {
            Employee employee = _dbContext.Employees.SingleOrDefault(emp => emp.Login == login);
            return employee;
        }

        public Employee GetEmployeeById(int? id)
        {
            Employee employee = _dbContext.Employees.SingleOrDefault(emp => emp.EmployeeId == id);
            return employee;
        }

        public void SetProfPic(int id)
        {
            var user = _dbContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
            if(user != null)
            {
                user.HasProfPic = true;
            }
            SaveChangesToDB();
        }

        public void SaveChangesToDB()
        {
            try
            {
                _dbContext.SaveChanges();
                return;
            }
            catch (DbUpdateException /* ex */)
            {       
                //TODO Log the error (uncomment ex variable name and write a log.)
                throw;
            }
        }
    }
}
