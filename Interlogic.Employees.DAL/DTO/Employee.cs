﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interlogic.Employees.DAL.DTO
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Alias { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime LastLoginOn { get; set; }
        public string ExtraFields { get; set; }
        public bool? HasProfPic { get; set; }
    }
}
