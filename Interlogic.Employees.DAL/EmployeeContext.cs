﻿using Interlogic.Employees.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interlogic.Employees.DAL
{
    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public EmployeeContext(DbContextOptions<EmployeeContext> options):base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().Property(e => e.CreatedOn).HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Employee>().Property(e => e.LastLoginOn).HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Employee>().Property(e => e.UpdatedOn).HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Employee>().Property(e => e.Role).HasDefaultValueSql("user");
            modelBuilder.Entity<Employee>().Property(e => e.IsActive).HasColumnName("active");
            modelBuilder.Entity<Employee>().Property(e => e.HasProfPic).HasColumnName("hasProfPic");
            
        }
    }
}
