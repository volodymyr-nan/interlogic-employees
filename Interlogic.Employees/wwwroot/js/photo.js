﻿
var Demo = (function () {
    var isSelected = false;
    var photoUrl = "";
    function output(node) {
        var existing = $('#result .croppie-result');
        if (existing.length > 0) {
            existing[0].parentNode.replaceChild(node, existing[0]);
        }
        else {
            $('#result')[0].appendChild(node);
        }
    }

    function demoUpload() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(input.files[0]);
            }
            else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            },
            enableOrientation: true,
            enableResize: true,
            showZoomer: false
        });

        $('#upload').on('change', function () {
            readFile(this);
            //$("#upload-msg").hide();
            $("#resultImg").hide();
            $(".upload-demo-wrap").show();
            $(".upload-result").show();
            isSelected = true;
        });

        $('.upload-result').on('click', function (ev) {
            console.log('upload click');
            if (isSelected) {
                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (resp) {
                    $("#resultImg").attr("src", resp);
                    $("#resultImg").show();
                    $(".upload-demo-wrap").hide();        
                    $('#imagebase64').val(resp);
                    $(".upload-result").hide();
                });
            }
            else {
                console.log("isSelected", isSelected);
                $('#form').submit(); 
            }
        });

        $('.accept-result').on('click', function (ev) {
            if (isSelected) {
                var src = $("#resultImg").attr('src');         
                $('#profilePhoto').attr("src", src);
            }
        });

        $('.syncBtn').on('click', function () {
            var url = "https://lh3.googleusercontent.com/-WY-fYG2kEgk/WW9DZg4CEPI/AAAAAAAAABQ/W9HTJgu2wc0h8tk9Hh9yLzLObCgZ5AgfwCEwYBhgL/w139-h140-p/03032017150413-0001.jpg";
            var xmlHTTP = new XMLHttpRequest();
            xmlHTTP.open('GET', url, true);
            xmlHTTP.responseType = 'arraybuffer';
            xmlHTTP.onload = function (e) {
                var arr = new Uint8Array(this.response);
                var raw = String.fromCharCode.apply(null, arr);
                var b64 = btoa(raw);
                var dataURL = "data:image/jpeg;base64," + b64;
                console.log("dataURL", dataURL);
                $('#imagebase64').val(dataURL);
            };
            xmlHTTP.send();
            $("#resultImg").attr("src", url);
            $("#resultImg").addClass("img-circle img-responsive");
            $("#resultImg").show();
            $(".upload-demo-wrap").hide();
            $(".upload-result").hide();
            isSelected = true;
        });

        $('.btn-save').on('click', function () {
            $('#form').submit();
        })
    }
  
    function init() {
        console.log('init');
        $("#resultImg").hide();
        $(".upload-demo-wrap").hide();
        $(".upload-result").hide();
        demoUpload();
    }

    return {
        init: init
    };
})();