﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interlogic.Employees.Helpers
{
    public static class JSONHelper
    {
        public static Dictionary<string, string> ConvertJsonStringToDictionary(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return new Dictionary<string, string>();
            }
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(value);
        }
        public static string ConvertDictionaryToJsonString(Dictionary<string, string> dictionary)
        {
            return JsonConvert.SerializeObject(dictionary);
        }
    }
}
