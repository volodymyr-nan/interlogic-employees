﻿using Interlogic.Employees.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interlogic.Employees.Controllers
{
    public class ApiController : Controller
    {
        private IEmployeeService _employeeService;

        public ApiController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public JsonResult Get()
        {
            return Json(_employeeService.GetAll());
        }
    }
}
