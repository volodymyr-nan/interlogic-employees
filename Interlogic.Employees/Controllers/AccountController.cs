﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interlogic.Employees.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        public AccountController()
        { }

        [HttpGet]
        public IActionResult Login(string returnUrl = "/")
        {
            returnUrl = Url.Action("ExternalLogin", new { returnUrl = returnUrl });
            return View("Consent", returnUrl);
        }


        public IActionResult ExternalLogin(string returnUrl, bool persistent)
        {
            returnUrl = Url.Action("ExternalLoginCallback", new { returnUrl = returnUrl });

            AuthenticationProperties props = new AuthenticationProperties
            {
                RedirectUri = returnUrl,
                Items = { { "scheme", "oidc" } },
                IsPersistent = persistent
            };
            return new ChallengeResult("oidc", props);
        }

        public IActionResult ExternalLoginCallback(string returnUrl)
        {
            return View("PostLogin", returnUrl);
        }

        public async Task Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Consent");
            await HttpContext.Authentication.SignOutAsync("Cookies");
            await HttpContext.Authentication.SignOutAsync("oidc");
        }
    }
}
