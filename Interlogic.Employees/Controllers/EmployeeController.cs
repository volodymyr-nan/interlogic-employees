﻿using Interlogic.Employees.DAL.DTO;
using Interlogic.Employees.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Interlogic.Employees.Models;


namespace Interlogic.Employees.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        private IEmployeeService _employeeService;
        private IHostingEnvironment _hostingEnv;

        public EmployeeController(IEmployeeService employeeService , IHostingEnvironment hostingEnv)
        {
            _employeeService = employeeService;
            _hostingEnv = hostingEnv;
            var s = _hostingEnv.ApplicationName;
        }

        public ViewResult Profile()
        {
            string login = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            EmployeeInfo employeeInfo = _employeeService.GetEmployeeByLogin(login);
            HttpContext.Session.SetInt32("EmployeeId", employeeInfo.EmployeeId);
            return View(employeeInfo);
        }

        public ViewResult EmployeeProfile(int? employeeId)
        {
            EmployeeInfo employeeInfo = _employeeService.GetEmployeeById(employeeId);
            return View("Profile", employeeInfo);
        }

        [ActionName("Edit")]
        public ViewResult EditEmployee(int? employeeId)
        {
            EmployeeInfo employeeInfo = _employeeService.GetEmployeeById(employeeId);
            return View(employeeInfo);
        }

        [HttpPost, ActionName("Edit")]
        public async Task<IActionResult> Edit(int? employeeId, string imagebase64)
        {
            if (employeeId == null)
            {
                return NotFound();
            }
            EmployeeInfo employeeInfo = _employeeService.GetEmployeeById(employeeId);
            if (await TryUpdateModelAsync(
                employeeInfo,
                "",
                s => s.ExtraFields))
            {
                try
                {
                    _employeeService.UpdateExtraField(employeeInfo);
                    if(imagebase64 != null)
                    {
                        _employeeService.SavePhoto(employeeId.Value, imagebase64, _hostingEnv.ContentRootPath);
                    }
                    return RedirectToAction("Profile");
                }
                catch
                {
                    //TODO Log the error
                    ViewBag.ErrorMessage = "Неможливо зберегти зміни. " +
                        "Спробуйте ще раз, і якщо проблема не зникне, " +
                        "зверніться до системного адміністратора.";
                }
            }
            return View(employeeInfo);
        }

        public ViewResult AllEmployees()
        {
            return View(_employeeService.GetAll().ToList());
        }

        public ViewResult AddRemoveExtraField()
        {
            string login = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            EmployeeInfo employeeInfo = _employeeService.GetEmployeeByLogin(login);
            ExtraField exctraField = new ExtraField(employeeInfo);
            return View(exctraField);
        }

        [HttpPost]
        public async Task<IActionResult> AddExtraField()
        {
            var employees = _employeeService.GetAll().ToList();
            ExtraField exctraField = new ExtraField(employees.First());
            if (await TryUpdateModelAsync(exctraField, "", s => s.Name, s => s.DefaultValue))
            {
                try
                {
                    _employeeService.AddExtraFieldForAllUsers(employees, exctraField.Name, exctraField.DefaultValue);
                    ViewBag.MessageAddSuccess = "Поле успішно додано";
                }
                catch
                {
                    //TODO Log the error
                    ViewBag.ErrorMessage = "Неможливо зберегти зміни. " +
                        "Спробуйте ще раз, і якщо проблема не зникне, " +
                        "зверніться до системного адміністратора.";
                }
            }
            return View("AddRemoveExtraField", exctraField);
        }

        [HttpPost]
        public IActionResult RemoveExtraField(IFormCollection form)
        {
            var employees = _employeeService.GetAll().ToList();
            try
            {
                string fieldName = form["DeletedFieldName"].ToString();
                _employeeService.RemoveExtraFieldForAllUsers(employees, fieldName);
                ViewBag.MessageDeleteSuccess = "Поле успішно видалено";
            }
            catch
            {
                //TODO Log the error
                ViewBag.ErrorMessage = "Неможливо зберегти зміни. " +
                        "Спробуйте ще раз, і якщо проблема не зникне, " +
                        "зверніться до системного адміністратора.";
            }
            ExtraField exctraField = new ExtraField(employees.First());
            return View("AddRemoveExtraField", exctraField);
        }
    }
}
