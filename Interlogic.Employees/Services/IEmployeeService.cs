﻿using Interlogic.Employees.DAL.DTO;
using Interlogic.Employees.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interlogic.Employees.Services
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeInfo> GetAll();
        EmployeeInfo GetEmployeeByLogin(string login);
        EmployeeInfo GetEmployeeById(int? id);
        void AddExtraField(EmployeeInfo employeeInfo, string fieldName, string defaultValue);
        void AddExtraFieldForAllUsers(IEnumerable<EmployeeInfo> employees, string fieldName, string defaultValue);
        void RemoveExtraField(EmployeeInfo employeeInfo, string fieldName);
        void RemoveExtraFieldForAllUsers(IEnumerable<EmployeeInfo> employees, string fieldName);
        void UpdateExtraField(EmployeeInfo employeeInfo);
        void SavePhoto(int employeeId, string photoUrl, string rootPath);
    }
}
