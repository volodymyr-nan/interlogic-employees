﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interlogic.Employees.DAL.DTO;
using Interlogic.Employees.DAL;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Interlogic.Employees.Models;
using Interlogic.Employees.Helpers;

namespace Interlogic.Employees.Services
{
    public class EmployeeService : IEmployeeService
    {
        private EmployeeAdapter _employeeAdapter;

        public EmployeeService(EmployeeContext context)
        {
            _employeeAdapter = new EmployeeAdapter(context);
        }

        public IEnumerable<EmployeeInfo> GetAll()
        {
            IEnumerable<Employee> employees = _employeeAdapter.GetAll().ToList();
            List<EmployeeInfo> employeesInfo = new List<EmployeeInfo>();
            foreach (var emp in employees)
            {
                employeesInfo.Add(emp);
            }
            return employeesInfo;
        }

        public EmployeeInfo GetEmployeeByLogin(string login)
        {
            return _employeeAdapter.GetEmployeeByLogin(login);
        }

        public EmployeeInfo GetEmployeeById(int? id)
        {
            return _employeeAdapter.GetEmployeeById(id);
        }

        public void AddExtraField(EmployeeInfo employeeInfo, string fieldName, string defaultValue)
        {
            employeeInfo.ExtraFields.Add(fieldName, defaultValue);
            Employee employeeToUpdate = _employeeAdapter.GetEmployeeById(employeeInfo.EmployeeId);
            employeeToUpdate.ExtraFields = JSONHelper.ConvertDictionaryToJsonString(employeeInfo.ExtraFields);
            _employeeAdapter.SaveChangesToDB();
        }

        public void RemoveExtraField(EmployeeInfo employeeInfo, string fieldName)
        {
            employeeInfo.ExtraFields.Remove(fieldName);
            Employee employeeToUpdate = _employeeAdapter.GetEmployeeById(employeeInfo.EmployeeId);
            employeeToUpdate.ExtraFields = JSONHelper.ConvertDictionaryToJsonString(employeeInfo.ExtraFields);
            _employeeAdapter.SaveChangesToDB();
        }

        public void UpdateExtraField(EmployeeInfo employeeInfo)
        {
            Employee employeeToUpdate = _employeeAdapter.GetEmployeeById(employeeInfo.EmployeeId);
            employeeToUpdate.ExtraFields = JSONHelper.ConvertDictionaryToJsonString(employeeInfo.ExtraFields);
            _employeeAdapter.SaveChangesToDB();
        }

        public void AddExtraFieldForAllUsers(IEnumerable<EmployeeInfo> employees, string fieldName, string defaultValue)
        {
            foreach (var emp in employees)
            {
                AddExtraField(emp, fieldName, defaultValue);
            }
        }

        public void RemoveExtraFieldForAllUsers(IEnumerable<EmployeeInfo> employees, string fieldName)
        {
            foreach (var emp in employees)
            {
                RemoveExtraField(emp, fieldName);
            }
        }

        public void SavePhoto(int employeeId, string photoUrl, string rootPath)
        {
         
            var BASE64_MARKER = ";base64,";
            var parts = photoUrl.Replace(BASE64_MARKER, " ").Split(' ');
            string base64 = parts[1];
            byte[] byteBuffer = Convert.FromBase64String(base64);
            using (MemoryStream memoryStream = new MemoryStream(byteBuffer))
            {
                memoryStream.Position = 0;
                using (Bitmap bmpReturn = new Bitmap(memoryStream))
                {
                    string path = String.Format("{0}photo_{1}.png", PathService.GetEmplFolderPath(rootPath),employeeId);
                    bmpReturn.Save(path, ImageFormat.Png);
                    _employeeAdapter.SetProfPic(employeeId);
                }
                byteBuffer = null;
            }
        }
    }
}
