﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interlogic.Employees.Services
{
    public class PathService
    { 
        public static string GetEmplFolderPath(string rootPath)
        {
            return String.Format("{0}\\wwwroot\\employees\\", rootPath);
        }

        public static string GetEmplWebPath()
        {
            return String.Format("employees\\");
        }

    }
}
