﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Interlogic.Employees.Models
{
    public class ExtraField
    {
        private string fieldName = String.Empty;

        [Required(AllowEmptyStrings = true, ErrorMessage = "Введіть назву поля")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Таке поле вже існує")]
        public string Name {
            get
            {
                return fieldName;
            }
            set
            {
                if (value == null)
                {
                    fieldName = value;
                    return;
                }

                if (!ExtraFields.ContainsKey(value))
                {
                    fieldName = value;
                }
            }
        }

        public string DefaultValue { get; set; }

        public Dictionary<string, string> ExtraFields { get; set; }

        public ExtraField(EmployeeInfo employeeInfo)
        {
            ExtraFields = employeeInfo.ExtraFields;
        }
    }
}
