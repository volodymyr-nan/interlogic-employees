﻿using Interlogic.Employees.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interlogic.Employees.Helpers;
using Interlogic.Employees.Services;

namespace Interlogic.Employees.Models
{
    public class EmployeeInfo
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Alias { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime LastLoginOn { get; set; }
        public Dictionary<string, string> ExtraFields { get; set; }
        //public bool HasProfPic { get; set; }
        public string PicturePath { get; set; }

        public EmployeeInfo(Employee employee)
        {
            EmployeeId = employee.EmployeeId;
            Name = employee.Name;
            Login = employee.Login;
            Alias = employee.Alias;
            Role = employee.Role;
            IsActive = employee.IsActive;
            Email = employee.Email;
            CreatedOn = employee.CreatedOn;
            UpdatedOn = employee.UpdatedOn;
            LastLoginOn = employee.LastLoginOn;
            ExtraFields = JSONHelper.ConvertJsonStringToDictionary(employee.ExtraFields);
            bool HasProfPic = employee.HasProfPic != null ? employee.HasProfPic.Value: false;
            PicturePath = PathService.GetEmplWebPath();
            PicturePath += HasProfPic ? String.Format("photo_{0}.png", employee.EmployeeId) : "default.png";
        }

        public static implicit operator EmployeeInfo(Employee obj)
        {
            return new EmployeeInfo(obj);
        }
    }
}
