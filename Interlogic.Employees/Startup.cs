﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Interlogic.Employees.DAL;
using Microsoft.EntityFrameworkCore;
using Interlogic.Employees.Services;
using Interlogic.Employees.Options;
using Microsoft.Extensions.Options;

namespace Interlogic.Employees
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("authenticationconfigs.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<EmployeeContext>(options => options.UseSqlServer(Configuration["ConnectionString"]));
            services.Configure<AuthenticationSettings>(Configuration.GetSection("auth"));

            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IOptions<AuthenticationSettings> config)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "Consent",
                LoginPath = new PathString("/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });


            app.UseStaticFiles();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies"
            });
            AuthenticationSettings auth = config.Value;
            OpenIdConnectOptions oidOptions = new OpenIdConnectOptions
            {
                AuthenticationScheme = "oidc",
                SignInScheme = "Cookies",

                Authority = auth.Authority,
                ClientId = auth.ClientId,
                ClientSecret = auth.ClientSecret,

                ResponseType = auth.ResponseType,

                RequireHttpsMetadata = false,
                GetClaimsFromUserInfoEndpoint = true,
                SaveTokens = true
            };
            foreach (string scope in auth.Scope)
            {
                oidOptions.Scope.Add(scope);
            }

            app.UseOpenIdConnectAuthentication(oidOptions);

            app.UseSession();

            app.UseMvc(routes =>
            {
                routes
                    .MapRoute(
                        "Default",
                        "{controller}/{action}/{employeeId?}",
                        new
                        {
                            controller = "Employee",
                            action = "Profile"
                        });
            });
        }
    }
}
